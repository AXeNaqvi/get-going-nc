<?php

define('AXETFCVERSION', '1.0.1');
define('AXETFCLANGDOMAIN', 'twentyfourteen');
define('AXETFCROOT', get_template_directory());
define('AXETFCPATH', trailingslashit(get_template_directory_uri()));
define('CURRENTAXETFCROOT', get_stylesheet_directory());
define('CURRENTAXETFCPATH', get_stylesheet_directory_uri());

function axetcf_setup()
{
    add_action('wp_enqueue_scripts', 'axetcf_scripts', 9999);
    remove_action('woocommerce_after_main_content', array('WC_Twenty_Fourteen', 'output_content_wrapper_end'));
    add_filter( 'gettext', 'axetcf_paypal_button_text', 20, 3 );
    add_theme_support( 'woocommerce', array(
        'thumbnail_image_width' => 250,
        'gallery_thumbnail_image_width' => 100,
        'single_image_width' => 600,
        ) );
    axetcf_registerNavs();
    axetcf_setDefaultSettings();
}
add_action('after_setup_theme', 'axetcf_setup', 9999);

function axetcf_scripts()
{
    /* Load our main stylesheet. */
    wp_dequeue_style('twentyfourteen-style');
    wp_dequeue_style('dashicons');
    wp_enqueue_style('dashicons');
    wp_enqueue_style('axetfparent-style', AXETFCPATH . '/style.css');
    /* wp_enqueue_style('getgoingnc-style', get_stylesheet_uri()); */
}

function axetcf_setDefaultSettings()
{
    $isloaded = get_option('axetcf_loaded');
    if (!$isloaded) {
        $settings = get_option('theme_mods_twentyfourteen');
        if (is_user_logged_in() && current_user_can('administrator')) {
            update_option('theme_mods_getgoingnc', $settings);
            add_option('axetcf_loaded', '1');
        }

    }
}

function axetcf_registerNavs()
{
    register_nav_menu('logoutmenu', __('Logged Out Menu', AXETFCLANGDOMAIN));
    register_nav_menu('loginmenu', __('Logged In Menu', AXETFCLANGDOMAIN));
}

function axetcf_loadTopNav()
{
    $menu = 'logoutmenu';
    if (is_user_logged_in()) {
        $menu = 'loginmenu';
    }
    wp_nav_menu(
        array(
            'theme_location' => $menu,
            'menu_class' => 'nav-axe-menu',
            'menu_id' => 'loginout-menu',
            'fallback_cb' => false,
        )
    );
}

function axetcf_paypal_button_text($translated_text, $text, $domain)
{
    switch ($translated_text) {
        case 'Proceed to PayPal':
            $translated_text = __('Proceed to Payment', 'woocommerce');
            break;
    }
    return $translated_text;
}
