<?php
/**
 * The WooCommerce template file
 */

get_header();?>

<div id="main-content" class="main-content axe-woo-content">

	<div id="primary" class="content-area">
		<div id="content" class="site-content" role="main">
        <?php
if (is_singular('product')) {
    woocommerce_content();
} else {
    do_action('woocommerce_before_main_content');

    ?>
			<header class="woocommerce-products-header">
				<?php if (apply_filters('woocommerce_show_page_title', true)): ?>
					<h1 class="woocommerce-products-header__title page-title"><?php woocommerce_page_title();?></h1>
				<?php endif;?>

				<?php
do_action('woocommerce_archive_description');
    ?>
			</header>
			<?php
if (woocommerce_product_loop()) {
        do_action('woocommerce_before_shop_loop');

        woocommerce_product_loop_start();

        if (wc_get_loop_prop('total')) {
            while (have_posts()) {
                the_post();
                do_action('woocommerce_shop_loop');
                wc_get_template_part('content', 'product');
            }
        }

        woocommerce_product_loop_end();
        do_action('woocommerce_after_shop_loop');
    } else {
        do_action('woocommerce_no_products_found');
    }

    do_action('woocommerce_after_main_content');
}
?>

		</div><!-- #content -->
	</div><!-- #primary -->

</div><!-- #main-content -->

<?php
get_footer();